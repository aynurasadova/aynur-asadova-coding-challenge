import React from 'react';
import {
    LineChart,
    BarChart,
    PieChart,
    ProgressChart,
    ContributionGraph,
    StackedBarChart
  } from "react-native-chart-kit";
import { Dimensions } from 'react-native';
import { color } from 'react-native-reanimated';

export const PieCharts = ({data, color}) => {
    return (
        <ProgressChart
            data={data}
            width={Dimensions.get("screen").width / 3 }
            height={220}
            strokeWidth={8}
            radius={40}
            chartConfig={{
                backgroundGradientFrom: "white",
                backgroundGradientFromOpacity: 1,
                backgroundGradientTo: "white",
                backgroundGradientToOpacity: 0.5,
                color: (opacity = 1) => `${color} ${opacity})`,
                strokeWidth: 2, // optional, default 3
                barPercentage: 0.5,
                useShadowColorFromDataset: false // optional
              }
            }
            hideLegend={true}
        />
    )
}