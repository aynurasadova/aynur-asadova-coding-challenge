export const transactionsDetails = {
    transactions: [
        {
            id: "1",
            fullName: "Ada Lovelace",
            amount: "$1300.50",
            // profilePhoto: ICONS.AdaLovelace,
            transactionDetails: {
                dateOfPayment: "30 May 20",
                cartType: "Cart",
                payWith: "Credit Account",
            }
        },
        {
            id: "2",
            fullName: "Mark Hopper",
            amount: "$720.25",
            // profilePhoto: ICONS.MarkHopper,
            transactionDetails: {
                dateOfPayment: "24 May 20",
                cartType: "Debt",
                payWith: "Credit Account",
            }
        },
        {
            id: "3",
            fullName: "Margaret Hamilton",
            amount: "$420.83",
            // profilePhoto: ICONS.MargaritHamilton,
            transactionDetails: {
                dateOfPayment: "15 May 2020",
                cartType: "Cart",
                payWith: "Private Account",
            }
        },
        {
            id: "4",
            fullName: "Ada Lovelace",
            // profilePhoto: ICONS.AdaLovelace,
            amount: "$1300.50",
            transactionDetails: {
                dateOfPayment: "25 April 20",
                cartType: "Debt",
                payWith: "Credit Account",
            }
        },
        {
            id: "5",
            // profilePhoto: ICONS.MarkHopper,
            fullName: "Mark Hopper",
            amount: "$720.25",
            transactionDetails: {
                dateOfPayment: "12 April 20",
                cartType: "Debt",
                payWith: "Credit Account",
            }
        },
        {
            id: "6",
            fullName: "Margaret Hamilton",
            // profilePhoto: ICONS.MargaritHamilton,
            amount: "$420.83",
            transactionDetails: {
                dateOfPayment: "8 April 2020",
                cartType: "Cart",
                payWith: "Private Account",
            }
        },
    ]
};