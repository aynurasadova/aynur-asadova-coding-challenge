import React from 'react';
import { StyleSheet, Text, View, StatusBar } from 'react-native';
import { RootNav } from './navigation/RootNav';
import { Provider } from 'react-redux';
import store from './redux/store';

export default function App() {
  return (
    <Provider store = {store}>
      <StatusBar hidden = {true} />
      <RootNav />
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
