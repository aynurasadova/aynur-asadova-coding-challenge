import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { View } from "react-native";
import { SingleTransactionDetail } from "../screens/SingleTransactionDetail";
import { HomeScreen } from "../screens/HomeScreen";

const { Navigator, Screen } = createStackNavigator();

export const RootNav = () => {
    return (
    <NavigationContainer>
      <Navigator headerMode = "none" >
      <Screen
        name="HomeScreen"
        component={ HomeScreen }
        /> 
      <Screen
        name="SingleTransactionDetail"
        component={ SingleTransactionDetail}
        />
      </Navigator>
    </NavigationContainer>
    )
}