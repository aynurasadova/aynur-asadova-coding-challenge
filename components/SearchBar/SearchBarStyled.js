import React from "react";
import { View, StyleSheet, TextInput, Image } from 'react-native';
import { ICONS } from "../../styles/icon";

export const SearchBarStyled = ({placeholderText, searchBarWidth = 100, ...rest}) => {
    return (
        <View style = {[styles.searchSection,{ width: `${searchBarWidth}%`}]}>
        <Image source = {ICONS.searchIcon} style={styles.searchIcon} />
        <TextInput
          placeholder = {placeholderText}
          {...rest}
        />
      </View>
    )
}

const styles = StyleSheet.create({
    searchSection: {
      flexDirection: "row",
      alignItems: "center",
      backgroundColor: "#fff",
      height: 35,
      borderRadius: 15,
      marginBottom: 30,
    },
    searchIcon: {
      margin: 10,
      width: 18,
      height: 18,
    },
  })
