export const searchHandler = (fieldValue, itemsToBeSearched) => {
    if (fieldValue === "") return itemsToBeSearched;
    else {
      let foundedItems = [];
      itemsToBeSearched.map((item) => {
        if (item.title?.toLowerCase().includes(fieldValue.toLowerCase()))
          foundedItems.push(item);
      });
      return foundedItems;
    }
};

export const formatData = (data, numColumns) => {
    const numberOfFullRows = Math.floor(data.length / numColumns);

    let numberOfElementsLastRow = data.length - numberOfFullRows * numColumns;
    while (
      numberOfElementsLastRow !== numColumns &&
      numberOfElementsLastRow !== 0
    ) {
      data.push({ empty: true });
      numberOfElementsLastRow++;
    }

    return data;
};
