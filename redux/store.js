import React from "react";
import { combineReducers, createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";

import { MODULE_NAME as transactionModuleName, reducer as transActionsReducer } from "./transactions";
import { AsyncStorage } from "react-native";
import { persistReducer, persistStore } from "redux-persist";

const config = {
  key: "root",
  storage: AsyncStorage,
};

const rootReducer = combineReducers({
  [transactionModuleName]: transActionsReducer,
});

const rootPersistReducer = persistReducer(config, rootReducer);

const store = createStore(
  rootPersistReducer,
  composeWithDevTools(applyMiddleware(thunk))
);

export const persistor = persistStore(store);

export default store;
