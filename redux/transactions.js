import { transactionsDetails } from "../demo-server/transactionsDetails";

const SET_TRANSACTION_DETAILS = "SET_TRANSACTION_DETAILS";

export const MODULE_NAME = "transActions";
export const selectAllTransactions = (state) => state[MODULE_NAME].transActions;
export const selectSingleTransactionDetails = (state, transactionID) => selectAllTransactions(state).find(transction => transction.id === transactionID);

const initialState = {
    transActions: [],
};

export function reducer(state = initialState, { type, payload }) {
    switch (type) {
        case SET_TRANSACTION_DETAILS:
            return {
                ...state,
                transActions: payload,
            }
        default:
            return state;
    }
}

export const setTransactionsDetails = (payload) => ({
    type: SET_TRANSACTION_DETAILS,
    payload,
});

export const getAndListenForDatas = () => (dispatch) => {
    try {
        if(transactionsDetails) {
            dispatch(setTransactionsDetails(transactionsDetails.transactions))
        } else {
            dispatch(setTransactionsDetails([]))
        }
    } catch (error) {
        console.log("getAndListenForDatas error", error.message)
    }
}
