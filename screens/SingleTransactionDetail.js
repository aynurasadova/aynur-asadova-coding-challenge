import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

import { connect } from 'react-redux';
import { selectSingleTransactionDetails } from '../redux/transactions';
import { GLOBAL_PADDING } from '../utils/GLOBAL_PADDING';


const mapStateToProps = (state, {route}) => ({
    singleTransaction: selectSingleTransactionDetails(state, route.params?.transactionID)
})

export const SingleTransactionDetail = connect(mapStateToProps)(({route, navigation, singleTransaction}) => {
    const { dateOfPayment, cartType, payWith} = singleTransaction?.transactionDetails || {};
    return (
        <View style = {styles.container}>
            <View style = {styles.header}>
                <TouchableOpacity onPress = {() => navigation.goBack()}>
                    <Text>
                        Back
                    </Text>
                </TouchableOpacity>
                <Text style = {styles.fullName}>{singleTransaction?.fullName || "Error 404"}</Text>
            </View>
            <Text style = {styles.amount}>{singleTransaction?.amount || "404"}</Text>
            <View style = {styles.cardTypes}>
                <TouchableOpacity style = {styles.cards}>
                    {/* <Image /> */}
                    <View style = {styles.cardTypeImg}/>
                    <Text style = {styles.name}>Card</Text>
                </TouchableOpacity>
                <TouchableOpacity style = {styles.cards}>
                    <View style = {styles.cardTypeImg}/>
                    <Text style = {styles.name}>Debt</Text>
                </TouchableOpacity>
                    
                
            </View>

            <Text style = {styles.detailsTxt}>Transaction Detail</Text>

            <View style = {styles.detailsWrapper}>
                <View>
                    <Text>Payment Detail</Text>
                    <View style = {styles.detailsInnerWrapper}>
                        <Text>{dateOfPayment}</Text>
                        <View style = {styles.icon}/>
                    </View>
                    <View style = {styles.detailsInnerWrapper}>
                        <Text>{cartType}</Text>
                        <View style = {styles.icon}/>
                    </View>
                    <View style = {styles.detailsInnerWrapper}>
                        <Text>{payWith}</Text>
                        <View style = {styles.icon}/>
                    </View>
                </View>
            </View>
            <View style = {styles.exportWrapper}>
                <View style = {styles.exportImg} />
                <Text>Export</Text>
            </View>
        </View>
    )
});

const styles = StyleSheet.create({
    container: {
        padding: GLOBAL_PADDING,
    },
    header: {
        flexDirection: "row",
        alignItems: "center",
        marginBottom: 50
    },
    fullName: {
        justifyContent: "center",
        alignSelf: "center",
        marginHorizontal: 78,
        fontWeight: "bold",
        fontSize: 18,
    },
    amount: {
        marginLeft: 95,
        fontSize: 50,
    },
    cardTypes: {
        flexDirection: "row",
        justifyContent: "space-between",
    },
    cardTypeImg: {
        width: 20,
        height: 20,
        borderRadius: 10,
        backgroundColor: "yellow"
    },
    cards: {
        flexDirection: "row",
        backgroundColor: "lightgrey",
        padding: 10,
        borderRadius: 10
    },
    name: {
        marginLeft: 10
    }
})