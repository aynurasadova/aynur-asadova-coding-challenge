import React, { useEffect, useState } from 'react';
import { View, TouchableOpacity, Text, FlatList, StyleSheet } from 'react-native';

import { connect } from 'react-redux';
import { selectAllTransactions, getAndListenForDatas } from '../redux/transactions';
import { GLOBAL_PADDING } from '../utils/GLOBAL_PADDING';
import { SearchBarStyled } from '../components/SearchBar/SearchBarStyled';
import { formatData, searchHandler } from '../components/SearchBar';
import { PieCharts } from '../componentsOfHomeScreen/PieCharts';

const mapStateToProps = (state) => ({
    transactions: selectAllTransactions(state),
})

export const HomeScreen = connect(mapStateToProps, {getAndListenForDatas})(({transactions, getAndListenForDatas, navigation}) => {
    
    const [value, setValue] = useState("");
    const [showCancel, setShowCancel] = useState(false);
    const [searchBarWidth, setSearchBarWidth] = useState(100);
    const onChangeText = (v) => {
        setSearchBarWidth(80);
        setShowCancel(true);
        setValue(v);
    };

    const handleCancel = () => {
        Keyboard.dismiss();
        setValue("");
    };

    useEffect(() => {
        if (value.trim() === "") {
        setShowCancel(false);
        setSearchBarWidth(100);
        }
    }, [value]);

    useEffect(() => {
        getAndListenForDatas()
    }, []);


    return (
        <View>
            <View style={styles.searchBarWrapper}>
                <SearchBarStyled
                value={value}
                onChangeText={(v) => onChangeText(v)}
                placeholderText="Search ..."
                searchBarWidth={searchBarWidth}
                />
                {showCancel && (
                <TouchableOpacity onPress={handleCancel}>
                    <Text style={styles.cancelText}>Cancel</Text>
                </TouchableOpacity>
                )}
            </View>
            <View style = {styles.chartWrapper}>
                <View>
                    <Text style = {styles.labels}>Current Week</Text>
                    <PieCharts data = {{
                            labels: ["Current Week"],
                            data: [0.64]
                        }} 
                        color = "rgba(26, 255, 146,"
                    />
                </View>

                <View>
                    <Text style = {styles.labels}>Last Week</Text>
                    <PieCharts data = {{
                            labels: ["Last Week"],
                            data: [0.4]
                        }} 
                        color = "rgba(196, 42, 31,"
                    />
                </View>
                <View>
                    <Text style = {styles.labels}>Last Month</Text>
                    <PieCharts data = {{
                        labels: ["Last Month"],
                        data: [0.9]
                    }} 
                    color = "rgba(30, 88, 232,"
                    />
                </View>
                
            </View>
            <FlatList 
                data={formatData(searchHandler(value, transactions), 1)}
                renderItem = {({item}) => (
                    <TouchableOpacity onPress = {() => navigation.navigate("SingleTransactionDetail", {transactionID: item.id})} style = {styles.transactionsContainer}>
                        <View style = {styles.leftCorner}>
                            <View style = {styles.image} />
                            <Text style = {styles.fullName}>{item.fullName}</Text>
                        </View>

                        <View style = {styles.rightCorner}>
                            <Text>{item.amount}</Text>
                        </View>
                        
                    </TouchableOpacity>
                )}
            />
        </View>
    )
});

const styles = StyleSheet.create({
    transactionsContainer: {
        paddingHorizontal: GLOBAL_PADDING,
        marginVertical: 10,
        flexDirection: "row",
        justifyContent: "space-between"
    },
    searchBarWrapper: {
        flexDirection: "row",
        fontSize: 20,
        justifyContent: "space-between",
        marginVertical: 20,
        marginHorizontal: GLOBAL_PADDING
      },
      chartWrapper: {
          flexDirection: "row",
          justifyContent: "center"
      },
      cancelText: {
        paddingVertical: 10,
        color: "lightgrey",
      },
    
    leftCorner: {
        flexDirection: "row",
        alignItems: "center"
    },
    image: {
        width: 45,
        height: 45,
        borderRadius: 25,
        backgroundColor: "lightgray"
    },
    fullName: {
        paddingLeft: GLOBAL_PADDING,
    },
    rightCorner: {
        flexDirection: "row",
        alignItems: "center"
    },
})